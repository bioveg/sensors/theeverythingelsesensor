#include "TcpCommunication.h"

using namespace Bioveg;

TcpCommunication::TcpCommunication(Relay& relay)
{
  TcpCommunication::relay = relay;
  m_subnet.fromString("255.255.255.0");
  m_dns.fromString(Parameters::gateway.c_str());
  m_ipAddress.fromString(Parameters::ip.c_str());
  m_gateway.fromString(Parameters::gateway.c_str());
  m_basestationIP.fromString(Parameters::basestationIP.c_str());
}

int TcpCommunication::SendDataToBasestation(const char* data, int dataType) {
  if (client.connect(m_basestationIP, (int)Parameters::basestationPort)) {
    client.print(dataType);
    client.print(",");
    client.println(data);
    client.flush();
    client.stop();
    return 1;
  }
  else
  {
    return 0;
  }
}

TaskFunction_t TcpCommunication::startServer(void *par) {
  server.begin();
  while (true)
  {
    WiFiClient serverClient = server.available();
    if (serverClient) {
      while (serverClient.connected())
      {
        if (serverClient.available())
        {
          ReadClient(serverClient);      
        }
      }
      serverClient.stop();
      serverClient.flush();
    }
  }
}

int TcpCommunication::WifiSetup() {
  m_ipAddress.fromString(Parameters::ip.c_str());
  WiFi.config(m_ipAddress, m_gateway, m_subnet,m_dns,m_dns);
  
  WiFi.begin(Parameters::ssid.c_str(), Parameters::pass.c_str());
  while (WiFi.status() != WL_CONNECTED)
  {
    delay(500);
  }
  static TaskHandle_t* t;
  // xTaskCreate(startServer, "startserver", 1000, this, 1, t);
  return 1;
}

void TcpCommunication::ReadClient(WiFiClient serverClient) {
  char buffer[bufferSize];
  for (size_t i = 0; i < bufferSize; i++)
  {
    buffer[i] = client.read();
  }
  relay.On(5);
}








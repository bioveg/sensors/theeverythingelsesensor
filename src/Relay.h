#include "Bioveg.h"

using namespace Bioveg;

namespace Bioveg
{
#ifndef Relay_h
#define Relay_h
    class Relay
    {
        public:
            Relay();
            Relay(int pin);
            void On();
            void Off();
            void On(int seconds);
        private:
            int pin;
    };

#endif
}
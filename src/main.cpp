#include "TcpCommunication.h"
#include "SoilSensor.h"
#include "Adafruit_Sensor.h"
#include "AirSensor.h"
#include "Relay.h"
#include "Bioveg.h"

using namespace Bioveg;

///C++ doing C++ things
const std::string Parameters::ssid = "BIOVEG_basestation";
const std::string Parameters::pass = "h6bioveg";
const std::string Parameters::ip = "10.10.10.102";
const std::string Parameters::gateway = "10.10.10.1";
const std::string Parameters::basestationIP = "10.10.10.1";
// convert float to string
Relay relay(Parameters::relayPinW);
TcpCommunication com = TcpCommunication(relay);
SoilSensor soilSensor;
AirSensor airSensor;

void setup() {
  pinMode(Parameters::relayPinW, OUTPUT);
  pinMode(Parameters::soilHumidPinR, INPUT);
  pinMode(Parameters::airClimatePinR, INPUT);
  while (com.WifiSetup() != 1)
  {
    ;
  }
}

void loop() {
  const char *t = Parameters::FtoC(airSensor.GetAirTemperature());
  com.SendDataToBasestation(t, (int)Parameters::sensorType::airTemp);
  t = Parameters::FtoC(airSensor.GetAirHumidity());
  com.SendDataToBasestation(t, (int)Parameters::sensorType::airHumid);
  t = Parameters::FtoC(soilSensor.GetSoilHumidity());
  com.SendDataToBasestation(t, (int)Parameters::sensorType::soilHumid);
  
  sleep(Parameters::sensorDelay);
}


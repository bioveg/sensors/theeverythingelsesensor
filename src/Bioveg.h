#include <string>
#include <sstream>
#include <iostream>
#include <Arduino.h>

namespace Bioveg 
{
#ifndef Bioveg_h
#define Bioveg_h
    class Parameters
    {
        public:
        //tcp stuff
            static const std::string ssid;
            static const std::string pass;
            static const std::string ip;
            static const std::string gateway;
            static const std::string basestationIP;
            static const int basestationPort{ 13000 };
            static const int serverPort{ 9999 };
        //pin things
            static const int soilHumidPinR{ 33 };
            static const int relayPinW{ 22 };
            static const int airClimatePinR{ 4 };
        //others
            enum class sensorType : int { airHumid, airTemp, soilHumid, soilTemp};
            //time between the sensor sending data in seconds
            static const int sensorDelay{ 10 };
        //helper function
            static const char *FtoC(float f);
        private:
            static std::ostringstream ss;
            static std::string s;
    };

#endif
}
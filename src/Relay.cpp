#include "Relay.h"

using namespace Bioveg;
Relay::Relay() {}

Relay::Relay(int pin) {
    Relay::pin = pin;
}
void Relay::On() {
    digitalWrite(pin, HIGH);
}
void Relay::Off() {
    digitalWrite(pin, LOW);
}
void Relay::On(int seconds) {
    On();
    sleep(seconds);
    Off();
}

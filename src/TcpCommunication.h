#include "WiFi.h"
#include "Relay.h"
#include "Bioveg.h"

using namespace Bioveg;
namespace Bioveg
{
    class TcpCommunication
    {
        
        public:
            TcpCommunication(Relay& relay);
            int WifiSetup();
            int SendDataToBasestation(const char* data, int dataType);
        private:
            Relay relay;
            IPAddress m_subnet;
            IPAddress m_dns;
            IPAddress m_ipAddress;
            IPAddress m_gateway;
            IPAddress m_basestationIP;
            WiFiServer server{ Parameters::serverPort };
            const int bufferSize{ 20 };
            WiFiClient client;
            void ReadClient(WiFiClient serverClient);
            TaskFunction_t startServer(void *par);
    };
}

#include "AirSensor.h"

using namespace Bioveg;
AirSensor::AirSensor()
{
    dht.begin();
}

float AirSensor::GetAirHumidity() {
///returns Air humidity in percentage
    return dht.readHumidity();
}

float AirSensor::GetAirTemperature() {
///returns Temperature in Celcius
    return dht.readTemperature();
}


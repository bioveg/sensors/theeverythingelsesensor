#include "Bioveg.h"
#include "DHT.h"

using namespace Bioveg;

namespace Bioveg
{
    class AirSensor
    {
        
        public:
            AirSensor();
            float GetAirTemperature();
            float GetAirHumidity();
        private:
            #define DHTTYPE DHT22
            // DHT dht{ (int)Parameters::airClimatePinR, DHTTYPE};
            DHT dht{ 4, DHTTYPE};

    };
}
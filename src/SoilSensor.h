#include "Bioveg.h"

using namespace Bioveg;
namespace Bioveg
{
    class SoilSensor
    {
        private:
            int sensorValue;
            
        public:
            SoilSensor();
            int GetSoilHumidity();
    };
}
    


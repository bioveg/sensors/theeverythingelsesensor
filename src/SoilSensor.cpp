#include "SoilSensor.h"

using namespace Bioveg;
SoilSensor::SoilSensor()
{
    
}

int SoilSensor::GetSoilHumidity() {
///returns soil humidity in percentage
    sensorValue = analogRead(Parameters::soilHumidPinR);
	// sensorValue = constrain(sensorValue,400,1023);
	sensorValue = map(sensorValue,4150,1690,0,100);
    return sensorValue;
}